<?php

namespace Asimov\FilesValidator\Traits;

trait importRows
{
    protected $rows = [];
    protected $headers = [];
    //$initial_row se debe setear en cada monitoreo en RequestValidations/Monitoring/
    protected $initial_row = 2;
    protected $header_row = 1;
    protected $calculatedCols = null;
    protected $format_valid = true;

    protected $active_sheet = null;

    protected function initialize($path){

        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);


        if ($this->format_valid){
            $file = \PhpOffice\PhpSpreadsheet\IOFactory::load(public_path($path));
           
            if (!is_null($this->active_sheet)){
                try {
                    $file->setActiveSheetIndexByName($this->active_sheet);
                    $this->rows = $file->getActiveSheet();
                } catch (\Exception $e){
                    $this->format_valid = false;
                    $this->undefinedSheet($this->active_sheet);
                }
            }else{
                $this->rows =  $file->getActiveSheet();
            }  
            
            $this->headers = $this->getHeaders();
        }
    }

    public function import(){
        if (!$this->format_valid){
            return $this->finalize();
        }

        $iterator = $this->rows->getRowIterator();
        $total = 0;

        foreach($iterator as $index => $row){
            if (count($this->traces) >= config('filesvalidator.max_traces')){
                break;
            }



            if($index >= $this->initial_row) {
                $celllist = $this->getRow($row, $index);
            
                /* Si encuentra más de cinco celdas vacías al inicio, lo toma como que
                 la fila está vacía y allí termina el archivo */
                if (is_null($celllist)){
                    break;
                }
                $total++;
                $this->captureRow($celllist, $index);

            }
        }

        if ($total == 0){
            $this->tracesFileEmpty();
        }

        return $this->finalize();
    }

    protected function getRow($celllist, $pos){
        $row = [];
        $iterator = $celllist->getCellIterator();
        $calculed = !is_null($this->calculatedCols);
        foreach($iterator as $index => $cell){
            $coor = $cell->getCoordinate();
            $cell_key = is_numeric(substr($coor, 1, 1)) ? substr($coor, 0, 1) : substr($coor, 0, 2);
            if ($calculed){
                if(in_array($cell_key, $this->calculatedCols)){
                    $value = $cell->getCalculatedValue();
                }else{
                    $value = $cell->getValue();
                }
            }else{
                $value = $cell->getValue();
            }

            if (strpos($value, '=') !== false && $cell_key != 'A' && $cell_key != 'B') {
                $this->reject($cell_key . $pos, 'La columna no permite formulas');
            }

            $value = $this->setEmptyToNull($value, $cell_key);
            $row[$cell->getCoordinate()] = $value;
        }

        $nulos = 0;
        foreach(['B', 'C', 'D', 'E', 'F'] as $coor){
            if (!isset($row[$coor . $pos])){
                $nulos++;
            }else if ($row[$coor . $pos] === null){
                $nulos++;
            }

            if ($nulos >= 4){
                return null;
            }
        }

        return $row;
    }

    protected function getValue($cell, $row, $index){
        return $row[$this->headers[$cell] . $index];
    }

    protected function getHeaders(){

        if (!$this->format_valid){
            return null;
        }

        $headers = [];
        $r_iterator = $this->rows->getRowIterator();
        $index = 1;
        foreach($r_iterator as $row){
            if ($index < $this->header_row){
                $index++;
                continue;
            }
            $c_iterator = $row->getCellIterator();
            foreach($c_iterator as $cell){
                $coor = $cell->getCoordinate();

                

                $cell_key = is_numeric(substr($coor, 1, 1)) ? substr($coor, 0, 1) : substr($coor, 0, 2);
                $headers[$cell_key] = $cell->getFormattedValue();
            }
            break;
        }

        return $headers;
    }

    //Está función convertirá todos los valores contenidos en la misma a null. Sobreescribela para personalizarla
    protected function setEmptyToNull($value, $cell_key){
        return $value;
    }

    //función llamada cuando se obtiene el contenido de una fila. Favor sobreescribir
    protected function captureRow($row, $index){

    }

    //función llamada cuando finaliza la importación del archivo. Favor sobreescribir
    protected function finalize(){

    }

    protected function undefinedSheet(){
        $this->reject('undefined_sheet', 'El archivo no contiene la hoja ' . $this->active_sheet . ', asegurese de que esté bien escrita y sin espacios antes o despues del texto.');
    }

    protected function tracesFileEmpty(){
        
    }

}