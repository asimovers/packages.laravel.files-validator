<?php

namespace Asimov\FilesValidator\Traits;

use Asimov\FilesValidator\Models\DocumentSchema;

trait SchemaFunctions
{

    public function list(){
        return DocumentSchema::all();
    }

    public function init($object){
        
        $this->validator = $object;
        $this->schema = DocumentSchema::where('classname', get_class($object))->first();
        return $this;
    }

    public function triggerTraces(){
        $this->triggerException = true;
        return $this;
    }

}