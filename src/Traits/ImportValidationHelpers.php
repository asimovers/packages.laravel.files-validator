<?php

namespace Asimov\FilesValidator\Traits;
use Illuminate\Support\Facades\App;

trait importValidationHelpers
{

    public function validate($validations){

        $listValidations = [];

        //Centraliza las validaciones
        foreach($validations as $cell => $line){
            $line = is_string($validations) ? explode('|', $line) :  $line;

            foreach ($line as $validation){
                if (empty($listValidations[$validation])){
                    $listValidations[$validation] = [];
                }
                $listValidations[$validation][] = $cell;
            }
        }

        //Irá ejecutandolas una por una
        foreach($listValidations as $validation => $listCells){
            $this->{$validation}($listCells);
        }
    }

    public function required($positions){

        foreach($positions as $pos){
            $coor = $pos . $this->index;

            if(!isset($this->row[$coor]) || strlen(trim($this->row[$coor])) == 0){
                $this->reject($coor, trans('asimov::validations.cell_required'));
            }
        }
    }

    public function email($positions){
        foreach($positions as $pos){
            $coor = $pos . $this->index;

            if (!isset($this->row[$coor])){
                continue;
            }

            if (!isset($this->row[$coor]) || !filter_var($this->row[$coor], FILTER_VALIDATE_EMAIL)) {
                $this->reject($coor, trans('asimov::validations.invalid_email'));
            }
        }
    }

    public function booleano($positions){
        foreach($positions as $pos){
            $coor = $pos . $this->index;

            if (!isset($this->row[$coor])){
                continue;
            }

            $posible_booleans = (App::isLocale('es')) ? 
                ['si', 'no', 'sí', 'no', 'Sí', 'No', 'SI', 'NO', 'SÍ', 'Si'] : 
                ['yes', 'no', 'not', 'Yes', 'No', 'YES', 'NO', 'Not', 'NOT'];
            if (!in_array(trim($this->row[$coor]), $posible_booleans)){
                $this->reject($coor, trans('asimov::validations.invalid_boolean'));
            }
        }
    }

    public function sumQuantities($positions, $target, $message, $error_range = null){
        $this->validateQuantities($positions, $target, $message,  $this->row, $this->index, function ($total, $target){
            return $total + $target;
        }, $error_range);
    }

    public function subtractQuantities($positions, $target, $message){
        $this->validateQuantities($positions, $target, $message,  $this->row, $this->index, function ($total, $target){
            return abs($total) - $target;
        });
    }

    public function multiplyQuantities($positions, $target, $message){
        $this->validateQuantities($positions, $target, $message,  $this->row, $this->index, function ($total, $target){
            return ($total === 0) ? (1 * $target) : ($total * $target);
        });
    }

    public function validateQuantities($positions, $target, $message, $callback, $error_range = null){
        $total = 0;

        foreach($positions as $pos){
            $coor = $pos . $this->index;
            if(!isset($this->row[$coor])){
                continue;
            }
            $total = $callback($total, floatval($this->row[$coor]));
        }

        $coor_target = $target . $this->index;

        if (!isset($this->row[$coor_target])){
            return;
        }

        $target_value = floatval($this->row[$coor_target]);

        $difference = $target_value - $total;

        $error_range = is_null($error_range) ? 1 : $error_range;

        if (abs($difference) > $error_range){
            $this->reject($coor, $message);
        }
    }

    public function number($positions){

        foreach($positions as $pos){
            $coor = $pos . $this->index;

            if (!isset($this->row[$coor]) || is_null($this->row[$coor])){
                continue;
            }

            if (!is_numeric($this->row[$coor])){
                $this->reject($coor, trans('asimov::validations.cell_number') );
            }
        }
    }

    public function number_range($position, $initial, $final){
        $coor = $position . $this->index;

        if (!isset($this->row[$coor])){
            return;
        }

        if (floatval($this->row[$coor]) >= $initial && $this->row[$coor] <= $final){
            return;
        }

        $this->reject($coor, trans('asimov::validations.invalid_range', ['initial' => $initial, 'final' => $final]));
    }



    public function custom($col, $function) {
        $coor = $col.$this->index;
        $res = $function($this->row, $coor);
        return $res['status'] ? true : $this->reject($coor, $res['message']);
    }

    public function getBoolean($data){

        return (mb_strtolower($data) == 'si' || mb_strtolower($data) == 'sí');
    }

    public function date($positions){
        if (!is_array($positions)){
            $this->validateDate($positions);
            return;
        }

        foreach($positions as $pos){
            $this->validateDate($pos);
        }
    }

    public function validateDate($col){

        $coor = $col . $this->index;


        try {
           $fecha = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($this->row[$coor]);
           if (!is_null($fecha)){
               return;
           }
        } catch (\Exception $e){

        }

        $this->reject($coor, trans('asimov::validations.invalid_date'));
    }

    public function validateSign($positions, $direction = true){

        foreach($positions as $pos){
            $coor = $pos . $this->index;
            $number = $this->row[$coor];

            if (!isset($number) || is_null($number) || !is_numeric($number)){
                continue;
            }

            if ($direction && $number < 0){
                $this->reject($coor, trans('asimov::validations.only_positive'));
                continue;
            }

            if (!$direction && $number > 0){
                $this->reject($coor, trans('asimov::validations.only_negative'));
                continue;
            }
        }
    }

    public function getDate($data){
        return is_null($data)
        ? $data : date('Y-m-d', \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($data));
    }

    public function getDateHour($data){
        return is_null($data)
        ? $data : date('Y-m-d h:i:s', \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($data));
    }

    public function setRequireds($positions, $requireds,  $isboolean = true){
        foreach($positions as $pos => $target){
            if (!$isboolean){
                if (!empty($this->row[$pos . $this->index])){
                    $requireds[] = $target;
                }

                continue;
            }

            if ($this->getBoolean($this->row[$pos . $this->index])){
                $requireds[] = $target;
            }
        }

        return $requireds;
    }

}
