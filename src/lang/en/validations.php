<?php
return [
    'file_required' => 'The file for the document :name is required',
    'file_formats' => 'File format not allowed. Only allowed: :formats',
    'cell_required' => 'The cell is required',
    'invalid_email' => 'Invalid email',
    'invalid_boolean' => 'Invalid value. Only Yes or No allowed',
    'cell_number' => 'The cell must be a number',
    'invalid_range' => 'Invalid value. Only accepted values between :initial and :final ',
    'invalid_register_male' => ':register does not correspond to those allowed',
    'invalid_register_female' => ':register does not correspond to those allowed',
    'invalid_date' => 'It can only be a date',
    'only_positive' => 'Only positive values are allowed',
    'only_negative' => 'Only negative values are allowed'
];
