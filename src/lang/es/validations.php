<?php
return [
    'file_required' => 'El archivo :name Es obligatorio',
    'file_formats' => 'Formato de archivo no permitido. Solo se permite: :formats',
    'cell_required' => 'La celda es requerida',
    'invalid_email' => 'Email invalido',
    'invalid_boolean' => 'Valor invalido. Solo se permite Sí o No',
    'cell_number' => 'La celda debe ser un número.',
    'invalid_range' => 'Valor invalido. Solo aceptados valores entre :initial y :final ',
    'invalid_register_male' => ':register no corresponde con los permitidos',
    'invalid_register_female' => ':register no corresponde con las permitidos',
    'invalid_date' => 'Solo puede ser una fecha',
    'only_positive' => 'Solo se permiten valores positivos',
    'only_negative' => 'Solo se permiten valores negativos'
];
