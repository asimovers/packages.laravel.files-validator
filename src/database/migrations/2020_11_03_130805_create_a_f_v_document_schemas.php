<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAFVDocumentSchemas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afv_document_schemas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('classname')->nullable();
            $table->string('formats', 255);
            $table->boolean('status')->default(true);
            $table->unsignedInteger('schema_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afv_document_schemas');
    }
}
