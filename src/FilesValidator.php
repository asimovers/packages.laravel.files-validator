<?php

namespace Asimov\FilesValidator;

use Illuminate\Support\Facades\DB;

use Asimov\FilesValidator\Traits\SchemaFunctions;

class FilesValidator {

    use SchemaFunctions;

    protected $triggerException = false;
    protected $path = null;
    protected $schema = null;
    protected $validator = null;
    protected $traces = [];

    function __construct(){
        set_time_limit(0);
        ini_set('memory_limit', -1);
    }


    //Métodos públicos

    public function save($path, $schema = null){
        $this->execute($path, $schema, true);
        return $this->getValidator();
    }

    public function validate($path, $schema = null){
        $this->execute($path, $schema, false);
        return $this->traces;
    }

    public function execute($path, $schema, $isSaving){

        $this->schema = !is_null($schema) ? $schema : $this->schema;
        $this->path = $path;

        DB::beginTransaction();
        $traces = $this->load($isSaving);

        if (count($traces) > 0){
            DB::rollBack();
            $this->throwTraces($traces);
        }   

        DB::commit();
    }


    //Métodos protegidos
    protected function load($isSaving){

        $traces = [];
        $arguments = [];

        if (is_null($this->validator)){
            $this->validator = new $this->schema->classname();
        }

        //Si tiene hojas el documento, iremos a por las hojas
        if ($this->schema->sheets->count() > 0){

            foreach($this->schema->sheets as $sheet){
                $traces[$this->schema->name] = $this->load($this->schema->sheets, $isSaving);
            }
            
        }else{
            //Preparar el objeto usado para la validación

            $this->validator->init($this->schema, $this->path, $isSaving);

            //Colocar los argumentos obtenidos desde el schema anterior, si los hay
            $this->validator->setArguments($arguments);

            //Ejecutar la importación
            $traces = $this->validator->import();

            //Salvamos los argumentos desde el objeto validador, si es que los hay, para pasarselos al siguiente schema, si es que lo hay
            $arguments = $this->validator->getArguments();
        }
            
        return $traces;
        
    
    }

    protected function throwTraces($traces){
        //Si no está activado triggerException, no rompas la ejecucion
        if ($this->triggerException){
            throw new \Exception(json_encode($traces));
        }

        $this->traces = $traces;
        
    }

    public function getTraces(){
        return $this->traces;
    }

    public function getValidator(){
        return $this->validator;
    }

}
