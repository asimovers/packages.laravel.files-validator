<?php

namespace Asimov\FilesValidator\Validators;

use Asimov\FilesValidator\Traits\importRows;
use Asimov\FilesValidator\Row;

class BaseExcelValidator extends BaseValidator {

    use importRows;
    protected $saving;

    protected function captureRow($row, $index){

        $rowObject = new Row($row, $index);

        if ($this->saving){
            $this->save($rowObject);
        }else{
            $row = $this->validate($rowObject);
            $this->traces = array_merge($this->traces, $row->getTraces());
        }
    }

    protected function tracesFileEmpty(){
        $this->reject('0', 'Archivo no contiene registros');
    }

    protected function finalize(){
        return $this->traces;
    }

    public function init($schema, $path, $saving){
        $this->initializeBase($schema, $path);
        $this->initialize($this->path);
        $this->saving = $saving;
        $this->prepare();
        
    }


    //sobreescribe esta función con tus validaciones especificas
    public function validate(Row $row){
        return $row;
    }

    //sobreescribe esta función para almacenaar los datos en la tabla respectiva
    public function save(Row $row){

    }

    //Función que se ejecuta antes de recorrer las celdas. Pon aquí lo que necesites inicializar
    public function prepare(){

    }

    //Las siguientes funciones sirven para intercambiar argumentos entre los distintos validadores. 

    //Con este se obtienen los argumentos del validador. sobreescribe devolviendo todos los datos que necesites pasar al siguiente validador.
    public function getArguments(){

    }

    //Con este se colocan los argumentos obtenidos en la función anterior, en el nuevo validador. Sobreescribe en el nuevo validador para colocarselos
    public function setArguments($arguments){

    }
}