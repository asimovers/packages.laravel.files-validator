<?php

namespace Asimov\FilesValidator\Validators;

class BaseValidator {

    protected $schema = null;
    protected $path = null;
    protected $traces = [];
    protected $format_valid = true;
    public $is_multisheet = false;

    protected function initializeBase($schema, $path){
        $this->schema = $schema;
        $this->path = $path;

        if (!$this->validateRequired()){
            return false;
        }

        $this->format_valid = $this->validateFormats();
    }

    private function validateRequired(){
        if (empty($this->path)){
            $this->reject('required', trans('asimov::validations.file_required', ['name' => $this->getSchemaAttr('name')]));
            return false;
        }

        return true;
    }

    private function validateFormats(){

        $formats = collect(explode(',', $this->getSchemaAttr('formats')));

        $error_format = true;

        foreach($formats as $format){
            if ($format == 'excel'){
                if(stripos(strtolower($this->path), 'xls') !== false || stripos(strtolower($this->path), 'xlsx') !== false){
                    $error_format = false;
                }
                continue;
            }

            if ($format == 'word'){
                if(stripos(strtolower($this->path), 'doc') !== false || stripos(strtolower($this->path), 'docx')){
                    $error_format = false;
                }
                continue;
            }

            if(stripos($this->path, $format) !== false){
                $error_format = false;
            }
        }

        if ($error_format){
            $this->reject('formats', trans('asimov::validations.file_formats', ['formats' => $this->getSchemaAttr('formats')]));
            return false;
        }

        return true;
    }

    private function getSchemaAttr($attr){
        return is_array($this->schema) ? $this->schema[$attr] : $this->schema->{$attr};
    }

    //llama a reject para indicar un error en la posición indicada
    protected function reject($coordinate, $message){
        $this->traces[$coordinate] = $message;
    }

    public function import(){
        return $this->finalize();
    }

    protected function finalize(){
        return $this->traces;
    }
}
