<?php

namespace Asimov\FilesValidator;

use Asimov\FilesValidator\Traits\importValidationHelpers;

class Row {

    protected $traces = [];
    protected $row = [];
    protected $index = null;

    use importValidationHelpers;

    function __construct($row, $index){
        $this->row = $row;
        $this->index = $index;
    }

    public function getTraces(){
        return $this->traces;
    }

    public function get($position){
        return $this->row[$position . $this->index];
    }

    public function set($position, $value){
        $this->row[$position . $this->index] = $value;
    }

    public function error($position, $message){
        $this->reject($position . $this->index, $message);
    }

    //llama a reject para indicar un error en la posición indicada
    protected function reject($coordinate, $message){
        $this->traces[$coordinate] = $message;
    }
}
