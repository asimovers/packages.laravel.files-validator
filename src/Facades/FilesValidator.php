<?php

namespace Asimov\FilesValidator\Facades;

use Illuminate\Support\Facades\Facade;

class FilesValidator extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'filesvalidator';
    }
}