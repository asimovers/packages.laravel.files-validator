<?php

namespace Asimov\FilesValidator\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

use Asimov\FilesValidator\Models\DocumentSchema;

class MakeAsimovValidator extends Command
{
    protected $signature = 'make-validator {classname}';

    protected $description = 'Creación de una nueva clase de validación de ficheros';
    protected $name;

    public function handle()
    {
        $classname = $this->argument('classname');
        $this->name = $this->ask('Ingresa el nombre para el validador');

        //Verificando el directorio
        $this->prepareDir();

        //Creando el archivo de la clase
        $this->makeClass($classname);

        //Creando el archivo de las constantes de la clase
        $this->makeConstants($classname);

        //Crea o actualiza el registro en la base de datos
        $this->createOrUpdateSchema($classname);
    }

    protected function prepareDir(){
        
        $file = new Filesystem();

        //Si no existe, lo crea
        $path = app_path('AsimovValidations/');
        $file->isDirectory($path) or $file->makeDirectory($path, 0777, true, true);

        //Hará lo mismo con la carpeta para las constantes
        $constPath = app_path('AsimovValidations/Constants/');
        $file->isDirectory($constPath) or $file->makeDirectory($constPath, 0777, true, true);
    }

    protected function createOrUpdateSchema($classname){

        $fullClassname = 'App\AsimovValidations\\' . $classname;
        $schema = DocumentSchema::where('classname', $fullClassname)->first();

        if (is_null($schema)){
            $schema = DocumentSchema::create([
                'name' => $this->name,
                'classname' => $fullClassname,
                'formats' => 'xls, xlsx, csv',
                'status' => true
            ]);

            return;
        }

        if (empty($name)){
            return;
        }

        $schema->name = $name;
        $schema->save();
    }

    protected function makeClass($classname){
        $path = app_path('AsimovValidations');

        $interfaceName = $classname . 'Constants';

        $content = "<?php

namespace App\AsimovValidations;
use Asimov\FilesValidator\Validators\BaseExcelValidator;
use Asimov\FilesValidator\Row;

use App\AsimovValidations\Constants\\$interfaceName As Constant;

class $classname extends BaseExcelValidator implements Constant {

    protected \$initial_row = 1;

    protected \$active_sheet = null;

    //Pasa por acá cualquier parámetro externo a la clase
    function __construct(){
       
    }

    //Coloca aquí cualquier proceso anterior al recorrido de las filas.
    public function prepare(){

    }
    
    //Coloca aquí las validaciones a cada fila.
    //OJO, lo colocado acá se ejecutará para cada una de las filas del archivo, así que abstente de hacer consultas SQL, para eso usa prepare()
    //Siempre debe retornar el objeto \$row
    public function validate(Row \$row){
        
        return \$row; 
    }
    
    //Coloca aquí el código para guardar los datos en la BD
    public function save(Row \$row){
        
    }
}";
        

        file_put_contents("{$path}/{$classname}.php", $content);
    }

    protected function makeConstants($classname){
        $path = app_path('AsimovValidations/Constants');

        $interfaceName = $classname . 'Constants';

        $content = "<?php
namespace App\AsimovValidations\Constants;

interface $interfaceName {

    //Archivo para definir constantes. Usalo para definir los nombres de las columnas y otros valores necesarios.

    const FIRST   = 'A';
    const SECOND  = 'B';
}
        ";

        file_put_contents("{$path}/{$interfaceName}.php", $content);
        
    }
}