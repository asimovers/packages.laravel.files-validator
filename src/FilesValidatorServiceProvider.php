<?php

namespace Asimov\FilesValidator;

use Illuminate\Support\ServiceProvider as Provider;
use Asimov\FilesValidator\Console\MakeAsimovValidator;

final class FilesValidatorServiceProvider extends Provider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //

        //Migrations
        $this->registerMigrations();

        //Resources
        $this->registerResources();

        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/config/config.php' => config_path('filesvalidator.php'),
            ], 'config');

            $this->commands([
                MakeAsimovValidator::class
            ]);
    
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(): void
    {
        //

        $this->mergeConfigFrom(__DIR__.'/config/config.php', 'filesvalidator');

        $this->app->bind('filesvalidator', function($app) {
            return new FilesValidator();
        });
    }


    /**
     * Register the package migrations
     *
     * @return void
     */
    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }

    /**
     * Register the package resources
     *
     * @return void
     */
    protected function registerResources(): void
    {
        //Load language translations...

        $this->loadTranslationsFrom(__DIR__.'/lang', 'asimov');

        $this->publishes([
            __DIR__.'/lang' => resource_path('lang/vendor/asimov'),
        ]);
    }
}
