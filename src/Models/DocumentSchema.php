<?php

namespace Asimov\FilesValidator\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentSchema extends Model
{
    //
    protected $table = 'afv_document_schemas';

    protected $fillable = ['name', 'classname', 'status', 'formats', 'schema_id'];

    //Hojas
    public function sheets(){
        return $this->hasMany(DocumentSchema::class, 'schema_id');
    }
}
